/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DescifraAes;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
/**
 *
 * @author mgarciab
 */
public class descifraAes {

    /**
     * @param args the command line arguments
     */
    /**
     * @param args the command line arguments
     */
    
    private static final int IV_SIZE = 16;
    private static final String ALGORITHM_AES = "AES/CBC/PKCS5Padding";
    private static final String ENCODING_UTF8 = "UTF-8";
    private static final String AES_KEY = "AES";
    private static final String ALGORITHM_HMAC = "HmacSHA256";

    /**
     * Encrypt with symmetrical key.
     *
     * @param aesKeyBase64
     * @param hmacKeyBase64
     * @param plainText
     * @return
     */

    public String encryptAes(String aesKeyBase64, String hmacKeyBase64, String valorCampo) {
        try {
            SecretKeySpec aesKey = new SecretKeySpec(Base64.getDecoder().decode(aesKeyBase64.getBytes(ENCODING_UTF8)), AES_KEY);
            SecretKeySpec hmacKey = new SecretKeySpec(Base64.getDecoder().decode(hmacKeyBase64.getBytes(ENCODING_UTF8)), ALGORITHM_HMAC);

            byte[] iv = generarInitializationVector();

            Cipher cipher = Cipher.getInstance(ALGORITHM_AES);
            cipher.init(Cipher.ENCRYPT_MODE, aesKey, new IvParameterSpec(iv));

            byte[] plainText = valorCampo.getBytes(ENCODING_UTF8);

            byte[] cipherText = cipher.doFinal(plainText);
            byte[] iv_cipherText = concatenateBytes(iv, cipherText);
            byte[] hmac = generarHMAC(hmacKey, iv_cipherText);
            byte[] iv_cipherText_hmac = concatenateBytes(iv_cipherText, hmac);

            byte[] iv_cipherText_hmac_base64 = Base64.getEncoder().encode(iv_cipherText_hmac);
            return new String(iv_cipherText_hmac_base64, ENCODING_UTF8);

        } catch (Exception e) {
            //throw new CifradoDescifradoException("Incidente al cifrar el parametro");
        }

        return valorCampo;

    }

    /**
     *
     * Decode with symmetrical key.
     *
     * @param aesKeyBase64
     * @param hmacKeyBase64
     * @param iv_cipherText_hmac
     * @return
     */
    public String decryptAes(String aesKeyBase64, String hmacKeyBase64, String valorCifrado) {
        try {
            SecretKeySpec aesKey = new SecretKeySpec(Base64.getDecoder().decode(aesKeyBase64.getBytes(ENCODING_UTF8)), AES_KEY);
            SecretKeySpec hmacKey = new SecretKeySpec(Base64.getDecoder().decode(hmacKeyBase64.getBytes(ENCODING_UTF8)), ALGORITHM_HMAC);

            int macLength = obtenerHMACLength(hmacKey);

            byte[] iv_cipherText_hmac = Base64.getDecoder().decode(valorCifrado.getBytes(ENCODING_UTF8));
            int cipherTextLength = iv_cipherText_hmac.length - macLength;

            byte[] iv = Arrays.copyOf(iv_cipherText_hmac, IV_SIZE);
            byte[] cipherText = Arrays.copyOfRange(iv_cipherText_hmac, IV_SIZE, cipherTextLength);
            byte[] iv_cipherText = concatenateBytes(iv, cipherText);
            byte[] receivedHMAC = Arrays.copyOfRange(iv_cipherText_hmac, cipherTextLength, iv_cipherText_hmac.length);
            byte[] calculatedHMAC = generarHMAC(hmacKey, iv_cipherText);

            if (receivedHMAC.length == calculatedHMAC.length) {
                Cipher cipher = Cipher.getInstance(ALGORITHM_AES);
                cipher.init(Cipher.DECRYPT_MODE, aesKey, new IvParameterSpec(iv));
                byte[] plainText = cipher.doFinal(cipherText);
                return new String(plainText, ENCODING_UTF8);
            } else {
                //throw new CifradoDescifradoException("HMAC Validation failed");
                return valorCifrado;
            }

        } catch (Exception e) {
            //throw new CifradoDescifradoException("Incidente al descifrar el parametro");
            //return valorCifrado;
        }
        return valorCifrado;

    }

    private int obtenerHMACLength(SecretKey key) throws NoSuchAlgorithmException, InvalidKeyException {

        Mac hmac = Mac.getInstance(ALGORITHM_HMAC);

        hmac.init(key);

        return hmac.getMacLength();

    }

    private byte[] generarInitializationVector() {

        byte[] iv = new byte[16];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(iv);
        return iv;
    }

    private byte[] concatenateBytes(byte[] first, byte[] second) {
        byte[] concatBytes = new byte[first.length + second.length];
        System.arraycopy(first, 0, concatBytes, 0, first.length);
        System.arraycopy(second, 0, concatBytes, first.length, second.length);
        return concatBytes;
    }

    public byte[] generarHMAC(SecretKey key, byte[] hmacInput) {
        Mac hmac;
        try {
            hmac = Mac.getInstance(ALGORITHM_HMAC);
            hmac.init(key);
            return hmac.doFinal(hmacInput);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return new byte[16];
    }

//    public SecretKey accesoSimetrico() {
//        KeyGenerator keyGenerator;
//        try {
//            keyGenerator = KeyGenerator.getInstance(AES_KEY);
//            keyGenerator.init(KEY_SIZE);
//            SecretKey clave = keyGenerator.generateKey();
//            return clave;
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
// 
//    public static boolean isNumeric(String cadena) {
// 
//        boolean resultado;
// 
//        try {
//            Integer.parseInt(cadena);
//            int id = Integer.parseInt(cadena);
//            if (id < 1) {
//                throw new ApiRestException(ApiRestError.SEG404_1002, Constante.API_SEGURIDAD_MENSAJE_ERROR_CODIGO,
//                        Constante.geneteFolio(), Arrays.asList(ApiRestError.SEG404_1002.getMensajeDetalleDefault()));
//            }
//            resultado = true;
//        } catch (NumberFormatException excepcion) {
//            throw new ApiRestException(ApiRestError.SEG400_1000, Constante.API_SEGURIDAD_MENSAJE_ERROR_CODIGO,
//                    Constante.geneteFolio(), Arrays.asList(ApiRestError.SEG400_1000.getMensajeDetalleDefault()));
//        }
//        return resultado;
//    }


}
